
var http = require("http");
var path = require("path");
var url = require("url");
var fs = require("fs");
var fsp = require("fs/promises");
const { basename } = require("path");

var { renderFile } = require("./tool/renderFile.js");



var { createServer } = http;


var server = createServer(function (req, res) {
    if (req.url != "/favicon.ico") {
        // console.log(req.url);  //接收到前端的请求地址 -> (请求的文件地址)

        var oUrl = url.parse(req.url, true);
        // console.log(oUrl);
        var { pathname, query } = oUrl;
        req.query = query; // 给req添加自定义属性 query -> 用于存储以get方式传递的数据

        console.log(pathname);

        // 前端页面展示
        var result = renderFile(pathname, res);  //默认循环站点内的文件 如果不是返回false
        if (result == false) {
            console.log("不是站点内的文件");

            // 路由
            if (pathname == "/a") {
                res.writeHead(200, {
                    "Content-Type": "text/plain;charset=utf-8"
                })
                res.end("这是a");
            } else if (pathname == "/reg") {
                renderFile("/html/register.html", res);
            }
        }









    } else {
        res.end();
    }
});


server.listen(5500, () => {
    console.log("Server is started at localhost:5500");
})