
var http = require("http");
var path = require("path");
var url = require("url");
var fs = require("fs");
var fsp = require("fs/promises");
const { basename } = require("path");

var { createServer } = http;

// var str = "http://localhost.com:5500/html/index.html?name=huaishuang&age=18#one";

// 将site作为服务器的默认站点路径
var defaultSite = path.join(__dirname, "../site");
var defaultEncode = "utf-8";

var server = createServer(function (req, res) {
    if (req.url != "/favicon.ico") {
        // console.log(req.url);  //接收到前端的请求地址 -> (请求的文件地址)

        var oUrl = url.parse(req.url, true);
        // console.log(oUrl);
        var { pathname, query } = oUrl;
        req.query = query; // 给req添加自定义属性 query -> 用于存储以get方式传递的数据

        console.log(pathname);

        var readPath = path.join(defaultSite, pathname);
        console.log(readPath);

        if (pathname == "/") {
            res.end("hello");
        } else if (fs.existsSync(readPath)) {
            console.log("11111");
            var ext = path.extname(pathname);
            console.log(ext);

            if ([".html"].includes(ext)) {
                fsp.readFile(readPath, defaultEncode).then(data => {
                    res.writeHead(200, {
                        "Content-Type": "text/html;charset=utf-8"
                    })
                    res.end(data);
                })
            } else if ([".css"].includes(ext)) {
                fsp.readFile(readPath, defaultEncode).then(data => {
                    res.writeHead(200, {
                        "Content-Type": "text/css;charset=utf-8"
                    })
                    res.end(data);
                })
            } else if ([".js"].includes(ext)) {
                fsp.readFile(readPath, defaultEncode).then(data => {
                    res.writeHead(200, {
                        "Content-Type": "text/javascript;charset=utf-8"
                    })
                    res.end(data);
                })
            } else if ([".jpg", ".jpeg", ".png", ".gif"].includes(ext)) {
                fsp.readFile(readPath, "binary").then(data => {
                    console.log(111111);
                    res.end(data, "binary");
                })
            }

        } else {
            console.log(2222);
            res.end("Not Found!")
        }






    } else {
        res.end();
    }
});


server.listen(5500, () => {
    console.log("Server is started at localhost:5500");
})