
// 1. 页面加载时只是绑定了事件,不执行
// 2. 等页面所有内容加载完毕之后 , 触发onload事件 => 执行对应的函数
// console.log(1);

// window.onload = function () {
//     console.log(2);

//     var box = document.getElementsByClassName("box")[0];
//     box.onclick = function () {
//         alert(111111);
//     }
// }

// console.log(3);