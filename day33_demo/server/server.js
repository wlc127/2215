
var http = require("http");
var path = require("path");
var url = require("url");
var fs = require("fs");
var fsp = require("fs/promises");
const { basename } = require("path");

var { renderFile } = require("./tool/renderFile");
var { router } = require("./tool/router.js");





var { createServer } = http;


var server = createServer(function (req, res) {
    if (req.url != "/favicon.ico") {
        // console.log(req.url);  //接收到前端的请求地址 -> (请求的文件地址)

        var oUrl = url.parse(req.url, true);
        // console.log(oUrl);
        var { pathname, query } = oUrl;
        req.query = query; // 给req添加自定义属性 query -> 用于存储以get方式传递的数据

        console.log(pathname);

        // 前端页面展示
        var res1 = renderFile(pathname, res);  //默认渲染站点内的文件 如果不是返回false
        var res2 = router(pathname, req, res); // 自定义路由

        if (res1 == false && res2 == false) {
            res.end("Not Found");
        }









    } else {
        res.end();
    }
});


server.listen(3000, () => {
    console.log("Server is started at localhost:3000");
})