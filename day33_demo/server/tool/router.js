
var path = require("path");
var fsp = require("fs/promises");

var { renderFile } = require("./renderFile.js");


function router(pathname, req, res) {

    // 路由
    // 1. 自定义路由显示页面  ->  路由跳转 
    // 2. 搭建后端接口(自定义路由) ->  前端向接口(路由)发送请求 -> 服务器返回对应的响应数据

    if (pathname == "/reg") {
        renderFile("/html/register.html", res);
    } else if (pathname == "/login") {
        renderFile("/html/login.html", res);
    } else if (pathname == "/search") {
        renderFile("/html/search.html", res);
    } else if (pathname == "/a") {
        res.writeHead(200, {
            "Content-Type": "text/plain;charset=utf-8"
        })
        res.end("这是a");
    } else if (pathname == "/pro") {
        res.writeHead(200, {
            "Content-Type": "text/json;charset=utf-8"
        })
        var list = {
            "country": "China",
            "list": [
                {
                    "city_id": "A001",
                    "city_name": "北京",
                    "list": [
                        {
                            "city_id": "A00101",
                            "city_name": "朝阳"
                        },
                        {
                            "city_id": "A00102",
                            "city_name": "海淀"
                        },
                        {
                            "city_id": "A00103",
                            "city_name": "上地"
                        },
                        {
                            "city_id": "A00104",
                            "city_name": "沙河"
                        }
                    ]
                },
                {
                    "city_id": "A002",
                    "city_name": "上海",
                    "list": [
                        {
                            "city_id": "A00201",
                            "city_name": "徐汇"
                        },
                        {
                            "city_id": "A00202",
                            "city_name": "静安"
                        },
                        {
                            "city_id": "A00203",
                            "city_name": "蓬莱"
                        },
                        {
                            "city_id": "A00204",
                            "city_name": "普陀"
                        }
                    ]
                },
                {
                    "city_id": "A003",
                    "city_name": "湖北",
                    "list": [
                        {
                            "city_id": "A00301",
                            "city_name": "武汉"
                        },
                        {
                            "city_id": "A00302",
                            "city_name": "孝感"
                        },
                        {
                            "city_id": "A00303",
                            "city_name": "襄阳"
                        },
                        {
                            "city_id": "A00304",
                            "city_name": "天门"
                        }
                    ]
                },
                {
                    "city_id": "A004",
                    "city_name": "湖南",
                    "list": [
                        {
                            "city_id": "A00401",
                            "city_name": "长沙"
                        },
                        {
                            "city_id": "A00402",
                            "city_name": "岳阳"
                        },
                        {
                            "city_id": "A00403",
                            "city_name": "常德"
                        },
                        {
                            "city_id": "A00404",
                            "city_name": "株洲"
                        }
                    ]
                }
            ]
        }
        res.end(JSON.stringify(list));
    } else if (pathname == "/grade") {
        res.writeHead(200, {
            "Content-Type": "text/json;charset=utf-8"
        })
        fsp.readFile(path.join(__dirname, "../data/grade.json"), "utf-8").then(data => {
            // data ->被fs读取 -> json字符串
            var { name } = req.query; //接收数据  -> 对name进行结构
            if (name) { // 有name数据
                var list = JSON.parse(data);
                var searchData = list.find(v => v.name == name);
                if (searchData) {
                    res.end(JSON.stringify(searchData));
                } else {
                    var obj = {
                        status: false,
                        message: "暂无数据"
                    }
                    res.end(JSON.stringify(obj));
                }
            } else {

                res.end(data);
            }

        })

    } else {
        return false;
    }
}

module.exports = {
    router,
}