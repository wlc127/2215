
var http = require("http");
var path = require("path");
var url = require("url");
var fs = require("fs");
var fsp = require("fs/promises");
const { basename } = require("path");

var { renderFile } = require("./tool/renderFile.js");
const { json } = require("stream/consumers");



var { createServer } = http;


var server = createServer(function (req, res) {
    if (req.url != "/favicon.ico") {
        // console.log(req.url);  //接收到前端的请求地址 -> (请求的文件地址)

        var oUrl = url.parse(req.url, true);
        // console.log(oUrl);
        var { pathname, query } = oUrl;
        req.query = query; // 给req添加自定义属性 query -> 用于存储以get方式传递的数据

        console.log(pathname);

        // 前端页面展示
        var result = renderFile(pathname, res);  //默认循环站点内的文件 如果不是返回false
        if (result == false) {
            console.log("不是站点内的文件");

            // 路由
            // 1. 自定义路由显示页面  ->  路由跳转 
            // 2. 自定义路由显示数据 ->  浏览器发送请求 服务器返回数据

            if (pathname == "/reg") {
                renderFile("/html/register.html", res);
            } else if (pathname == "/login") {
                renderFile("/html/login.html", res);
            } else if (pathname == "/search") {
                renderFile("/html/search.html", res);
            } else if (pathname == "/a") {
                res.writeHead(200, {
                    "Content-Type": "text/plain;charset=utf-8"
                })
                res.end("这是a");
            } else if (pathname == "/pro") {
                res.writeHead(200, {
                    "Content-Type": "text/json;charset=utf-8"
                })
                var list = {
                    "country": "China",
                    "list": [
                        {
                            "city_id": "A001",
                            "city_name": "北京",
                            "list": [
                                {
                                    "city_id": "A00101",
                                    "city_name": "朝阳"
                                },
                                {
                                    "city_id": "A00102",
                                    "city_name": "海淀"
                                },
                                {
                                    "city_id": "A00103",
                                    "city_name": "上地"
                                },
                                {
                                    "city_id": "A00104",
                                    "city_name": "沙河"
                                }
                            ]
                        },
                        {
                            "city_id": "A002",
                            "city_name": "上海",
                            "list": [
                                {
                                    "city_id": "A00201",
                                    "city_name": "徐汇"
                                },
                                {
                                    "city_id": "A00202",
                                    "city_name": "静安"
                                },
                                {
                                    "city_id": "A00203",
                                    "city_name": "蓬莱"
                                },
                                {
                                    "city_id": "A00204",
                                    "city_name": "普陀"
                                }
                            ]
                        },
                        {
                            "city_id": "A003",
                            "city_name": "湖北",
                            "list": [
                                {
                                    "city_id": "A00301",
                                    "city_name": "武汉"
                                },
                                {
                                    "city_id": "A00302",
                                    "city_name": "孝感"
                                },
                                {
                                    "city_id": "A00303",
                                    "city_name": "襄阳"
                                },
                                {
                                    "city_id": "A00304",
                                    "city_name": "天门"
                                }
                            ]
                        },
                        {
                            "city_id": "A004",
                            "city_name": "湖南",
                            "list": [
                                {
                                    "city_id": "A00401",
                                    "city_name": "长沙"
                                },
                                {
                                    "city_id": "A00402",
                                    "city_name": "岳阳"
                                },
                                {
                                    "city_id": "A00403",
                                    "city_name": "常德"
                                },
                                {
                                    "city_id": "A00404",
                                    "city_name": "株洲"
                                }
                            ]
                        }
                    ]
                }
                res.end(JSON.stringify(list));
            } else if (pathname == "/grade") {
                res.writeHead(200, {
                    "Content-Type": "text/json;charset=utf-8"
                })
                fsp.readFile(path.join(__dirname, "./data/grade.json"), "utf-8").then(data => {
                    // data ->被fs读取 -> json字符串
                    var { name } = req.query; //接收数据  -> 对name进行结构
                    if (name) { // 有name数据
                        var list = JSON.parse(data);
                        var searchData = list.find(v => v.name == name);
                        if (searchData) {
                            res.end(JSON.stringify(searchData));
                        } else {
                            var obj = {
                                status: false,
                                message: "暂无数据"
                            }
                            res.end(JSON.stringify(obj));
                        }
                    } else {

                        res.end(data);
                    }

                })

            } else {
                res.statusCode = 404;
                res.statusMessage = "Not Found";
                res.end("Not Found");
            }
        }









    } else {
        res.end();
    }
});


server.listen(3000, () => {
    console.log("Server is started at localhost:5500");
})