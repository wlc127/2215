// 使用import命令的时候，用户需要知道所要加载的变量名或函数名，否则无法加载。但是，用户肯定希望快速上手，未必愿意阅读文档，去了解模块有哪些属性和方法。

// 为了给用户提供方便，让他们不用阅读文档就能加载模块，就要用到export default命令，为模块指定默认输出。


export var a = 10;
export var b = 20;
export var c = a + b;

// export { a, b, c}

// export default 40;  //设置默认值  (一个模块不能具有多个默认导出)
// export default function () {
//     return a + b + c;
// };

export default {
    a,
    b,
    c,
}


