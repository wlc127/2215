

var x = 10;
var y = 20;
function add() { 
    return x + y;
}

// 模块化暴露时(批量暴露)  可以修改被暴露的变量名和方法名
export { x as X, y as Y ,add as sum};