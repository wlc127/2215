
// 一个模块就是一个独立的文件。该文件内部的所有变量，外部无法获取
// 如果模块中的部分变量和方法向被其他模块访问 -> 就需要先通过export关键词 暴露变量和方法

// 如何暴露?
// 1. 单独暴露  => 在变量和方法的声明之前添加关键词 export

console.log("模块1.js被引入");

export var a = 10;

export function addA() {
    a++;
    return a;
}

export function reduceA() {
    a--;
    return a;
}


export function getA() { 
    return a;
}
function setA(val) { 
    a = val;
    return a;
}




