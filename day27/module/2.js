
// 一个模块就是一个独立的文件。该文件内部的所有变量，外部无法获取
// 你希望外部能够读取模块内部的某个变量，就必须使用export关键字输出该变量和方法

// 如何暴露?
// 1. 单独暴露  => 在变量和方法的声明之前添加关键词 export
// 2. 批量暴露  => 在export命令后面，使用大括号指定所要输出的一组变量

console.log("模块2.js被引入");

var b = 20;

function addB() {
    b++;
    return b;
}

function reduceB() {
    b--;
    return b;
}

function getB() { 
    return b;
}

export { b, addB, reduceB,getB };




