

// (3) 可以在一个模块中 引入另一个或多个模块

import { a,getA } from "./1.js";
import { b,getB } from "./2.js";

export var c = a + b;
export function getC() { 
    return getA() + getB();
};

// 注意:
// export 暴露的是变量,函数,类的声明 而不是值

// var c = a + b;
// export c;

// function fn() { 

// }
// export fn;
