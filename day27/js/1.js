
// var a = 10;

// function add() {
//     a++;
//     return a;
// }

// function reduce() {
//     a--;
//     return a;
// }

// function getA() {
//     return a;
// }

// function setA(val) {
//     a = val;
//     return a;
// }

// var obj = {
//     a: 10,
//     add() {
//          this.a++;
//         return this.a;
//     },
//     reduce() {
//          this.a--;
//         return  this.a;
//     },
//     getA() {
//         return  this.a;
//     },
//     setA(val) {
//          this.a = val;
//         return  this.a;
//     },
// }

// 闭包
function fn() {
    var a = 0;

    // Object.defineProperty({},"a",{
    //     get(){ },
    //     set(){ }
    // })
    return {
        // a: 10, // 10

        get a() { // 对 对象中的属性a取值
            return a; // 父作用域中的变量a
        },
        // set a(val) {
        //     a = val;
        //     return a;
        // },

        add: function () {
            a++;
            return a;
        },
        reduce: function () {
            a--;
            return a;
        },
       
    }
}