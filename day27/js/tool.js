

 function throttleFn(callbackFn, delay = 0) {
    var start = Date.now();
    return function (...args) {
        var now = Date.now();
        if (now - start >= delay) {
            callbackFn.apply(this, args);
            start = now; // 下次执行 要比当前时间 多至少300ms
        }
    }
}

function debounceFn(callbackFn, delay = 0) {
    var dTimer = null;
    // 返回新函数 (防抖处理) -> 传入的回调函数(原函数)
    return function (...args) { //将新函数接收的所有参数整合到数组中
        clearTimeout(dTimer);
        dTimer = setTimeout(() => {
            // callbackFn(); // this -> window   无参数
            callbackFn.apply(this, args);
        }, delay);
    }
}