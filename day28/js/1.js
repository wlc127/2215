

/**
 * 这是一个加法计算函数
 *
 * @param {number} a 第一个数字
 * @param {number} b 第二个数字
 * @returns  两数之和
 */

function add(a, b) { 
    return a + b;
}



var x = 10;

function changeX(val) { 
    x = val;
}

export { x , changeX};
