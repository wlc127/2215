
var express = require("express");  // 第三方模块 -> 下载之后直接引用

var app = express(); // 创建的服务(server -> app)

// app.use()  中间件
// 1. 每次客户端发送请求到服务端 会依次经过每一个中间件(前提响应没有结束)
// 2. 对指定路由进行拦截 -> 执行对应的中间件函数

// 常见应用:
// 执行任何代码。
// 对请求和响应对象进行更改。
// 结束请求-响应循环。0
// 调用堆栈中的下一个中间件。

// 第一种应用 -> 对所有请求进行拦截
// 第二种应用 -> 对指定路由进行拦截
// 1. 所有的路由 都会被app.use("/")  根路由拦截
// 2. 访问子级路由时,也会触发父级路由的拦截

// 对所有请求进行拦截
app.use(function (req, res, next) {
    // console.log(req.method, req.url, req.headers);
    if (req.url == "/favicon.ico") {
        res.end();
    } else {
        next();
    }
})


app.use(function (req, res, next) {
    // console.log(req.method, req.url, req.headers);
    console.log(1111);
    // res.end("1111");
    next();

})
app.use(function (req, res, next) {
    // console.log(req.method, req.url, req.headers);
    console.log(2222);
    next();
})

// 拦截指定路由
// /data -> 拦截一级路由为/data 及其子路由 /data/goods   /data/goods/a/b/c
app.use("/data", function (req, res, next) {
    // console.log(req.method, req.url, req.headers);
    console.log(3333);
    next();
})

app.use("/data/goods", function (req, res, next) {
    // console.log(req.method, req.url, req.headers);
    console.log(4444);
    next();
})



app.use(function (req, res, next) {
    res.end();
})







app.listen(3000, () => {
    console.log("Server is Started at localhost:3000");
})