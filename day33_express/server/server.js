
var express = require("express");  // 第三方模块 -> 下载之后直接引用
var path = require("path");

var bodyParser = require("body-parser"); // 第三方 (post数据解析)

var app = express(); // 创建的服务(server -> app)

// 中间件

// app.use(function (req, res, next) {
//     // 数据解析
// })

// post数据解析
app.use(bodyParser.urlencoded({ extended: true }));   // 用于解析application/x-www-form-urlencoded
app.use(bodyParser.json()); //用户解析 application/json

// 指定站点位置 (可以指定多个)
app.use(express.static(path.join(__dirname, "../site")));
// app.use(express.static(path.join(__dirname, "../upload")));

// 配置路由
var IndexRouter = require("./router/index.js");
var DataRouter = require("./router/data.js");
var RegRouter = require("./router/register.js");

app.use(IndexRouter);

// /data 可以对一级路由 /data 及其子路由进行拦截
// /data    /         => /data
// /data    /goods    => /data/goods
// /data    /grade    => /data/grade
// /data    /grade/2215    => /data/grade/2215
// /data    /list     => /data/list

app.use("/data", DataRouter);
app.use("/reg", RegRouter);

app.listen(3000, () => {
    console.log("Server is Started at localhost:3000");
})