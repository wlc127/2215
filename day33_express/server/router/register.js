
var express = require("express");
var path = require("path");

var Router = express.Router();  //创建一个路由

// 可以对指定路由进行拦截
// Router.get(url)   拦截以get方式向指定路由发送的请求
// Router.post(url)  拦截以post方式向指定路由发送的请求
// Router.all(url)   拦截以所有向指定路由发送的请求

// req.query  接收以get方式传递的数据(对象)
// req.body   接收以post方式传递的数据(对象)  -> 需要下载第三方插件做数据解析

// 根路由
// /reg  => 自定义路由显示页面 

var defaultSite = path.join(__dirname, "../../site");

Router.get("/", function (req, res, next) {
    console.log("/reg");
    res.sendFile(defaultSite + "/html/register.html"); // 返回给定路径的文件传输数据 -> 返回给浏览器
})


module.exports = Router; // 返回路由
