
var express = require("express");

var Router = express.Router();  //创建一个路由

// 可以对指定路由进行拦截
// Router.get(url)   拦截以get方式向指定路由发送的请求
// Router.post(url)  拦截以post方式向指定路由发送的请求
// Router.all(url)   拦截以所有向指定路由发送的请求

// req.query  接收以get方式传递的数据(对象)
// req.body   接收以post方式传递的数据(对象)  -> 需要下载第三方插件做数据解析

// 根路由
Router.get("/", function (req, res, next) {
    console.log(1111);
    next();
})
Router.get("/", function (req, res, next) {
    console.log(2222);
    res.end("/")
})

// 一级路由
Router.get("/req", function (req, res, next) {
    console.log("get", req.query);
    var { user, pwd } = req.query;
    if (user && pwd) {  // 传入数据
        var data = { status: true, message: "OK!" }
        res.status(200).json(data);
    } else {  // 没有传入指定数据
        var data = { status: false, message: "请传入完整参数" };
        // res.end(JSON.stringify(data)); // 对象转JSON字符串后返回
        res.status(200).json(data);
    }
})

Router.post("/req", function (req, res, next) {
    console.log("post", req.body);
    var { user, pwd } = req.body;
    if (user && pwd) {  // 传入数据
        var data = { status: true, message: "OK!" }
        res.status(200).json(data);
    } else {  // 没有传入指定数据
        var data = { status: false, message: "请传入完整参数" };
        // res.end(JSON.stringify(data)); // 对象转JSON字符串后返回
        res.status(200).json(data);
    }
})




Router.get("/aaa", function (req, res, next) {
    console.log(2222);
    res.end("/aaa")
})

Router.get("/bbb", function (req, res, next) {
    console.log(2222);
    res.end("/bbb")
})
// 二级路由
Router.get("/aaa/bbb", function (req, res, next) {
    console.log(2222);
    res.end("/aaa/bbb")
})



module.exports = Router; // 返回路由
