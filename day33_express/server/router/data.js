
var express = require("express");

var Router = express.Router();  //创建一个路由

// 可以对指定路由进行拦截 (get post all)
// 根路由

// /data/
Router.get("/", function (req, res, next) {
    res.end("/data")
})

Router.get("/goods", function (req, res, next) {
    res.end("/data/goods")
})

Router.get("/grade", function (req, res, next) {
    res.end("/data/grade")
})

Router.get("/grade/2215", function (req, res, next) {
    res.end("/data/grade/2215")
})
Router.get("/list", function (req, res, next) {
    res.end("/data/list")
})





module.exports = Router; // 返回路由
