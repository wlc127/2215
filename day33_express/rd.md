# 基于 express 搭建 node 服务器

express 属于第三方模块 -> 先下载 在引入 再使用

## 初始化 package.json

考虑到需要下载的第三方模块比较多 -> 项目搭建之初 -> 初始化一个 package.json
a. 记录下载的模块 (开发依赖 生成依赖(运行依赖))
b. 反向安装 -> 根据 package.json 文件 下载所需要的模块

```
   npm init   初始化
   npm install  反向安装
```

## 安装 cnpm 或 nrm 切换国内下载源

node 的服务器在国外 通过 npm 下载各种包的时候 需要翻墙 慢...
解决 1: 翻墙问题(不需要) => 淘宝在国内搭建服务器 同步国外的包(淘宝镜像) => 只需将下载源改为国内

```
npm install -g cnpm --registry=https://registry.npmmirror.com    //切换下载源
npm install -g cnpm@6.2 --registry=https://registry.npmmirror.com    //切换下载源(低版本)
```

注意: 使用 cnpm 替换 npm

```
   cnpm install jquery -S
```

解决方法 2: node 的第三方模块中 nrm(全局安装) -> 切换下载源

```
   npm install nrm -g       下载nrm(命令程序)
   nrm ls                   列举下载源
   nrm test    列举出所有的资源的请求时间
   nrm use taobao           切换至taobao
```

后续可以直接使用 npm

## 下载 express

express 属于生成依赖(运行依赖) -S

```
npm install express -S
```
