
let mysql = require("mysql");

// 创建连接 -> 连接mysql -> 配置信息  -> 返回连接对象
var conn = mysql.createConnection({
    host:"localhost", // 本地主机
    port:'3306',   // 端口 默认3306
    user:"root",  // 登录的用户名
    password:"root", // 密码
    database:"2215"
})
// console.log(conn);

// conn.connect() ; // 开启链接
// conn.end() ; // 关闭链接 (sql语句执行完毕之后需要关闭链接)

// conn.query(sql,callback(){}) 执行传入的sql语句

// mysql执行查询语句 -> 查询成功=>结果(数组)  查询失败-> [] 
// var sql = "select id,name,class,chinese,math,english,chinese+math+english as total from grade where class = '2216'";
// conn.query(sql,function(err,results){
//     if(err){
//         throw err;
//     }
//     console.log("查询成功:", results);
//     conn.end();  //查询结束 -> 关闭链接
// })


// mysql执行增删改 -> 结果对象(包含 affectedRows(受影响的行数) insertId: 新增的数据的id)
var sql = "insert into grade(name,class,chinese,math,english) values('张四','2215',55,66,77);"
conn.query(sql,function(err,results){
    if(err) throw err; // err对象 (sqlMessage: sql提示 sql:sql语句)
    console.log("新增成功:",results);
    conn.end();
})


// 查询的结果时基于回调函数返回的   -> 封装为Promise

