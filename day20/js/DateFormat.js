
// 日期格式化
// pattern 传入的模板 => 期望输出的格式
function dateFormat(pattern, inputTime) { // "YYYY-MM-DD hh:mm:ss"

    if (pattern == undefined) pattern = "YYYY-MM-DD hh:mm:ss";

    if (inputTime == undefined) {
        var date = new Date();
    } else {
        var date = new Date(inputTime);
    }

    var year = date.getFullYear();
    var month = date.getMonth() + 1;  //展示到页面中
    var day = date.getDate();

    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();

    return pattern.replace("YYYY", year).replace("MM", beauty(month)).replace("DD", beauty(day)).replace("hh", beauty(hour)).replace("mm", beauty(minute)).replace("ss", beauty(second))
}


function beauty(num) {
    return num < 10 ? "0" + num : num;
}