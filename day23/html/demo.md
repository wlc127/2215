# MarkDown 介绍

## 简介

Markdown 是一种轻量级标记语言，它允许人们使用易读易写的纯文本格式编写文档。
Markdown 语言在 2004 由约翰·格鲁伯（英语：John Gruber）创建。
Markdown 编写的文档可以导出 HTML 、Word、图像、PDF、Epub 等多种格式的文档。
Markdown 编写的文档后缀为 .md, .markdown。

## 标题

使用 # 号可表示 1-6 级标题，一级标题对应一个 # 号，二级标题对应两个 # 号，以此类推。

## 字体

_斜体文本_
_斜体文本_
**粗体文本**
**粗体文本**
**_粗斜体文本_**
**_粗斜体文本_**

## Markdown 列表

- 选项 1
- 选项 2
- 选项 3

1. 选项 1
2. 选项 2
3. 选项 3

## Markdown 区块

Markdown 区块引用是在段落开头使用 > 符号 ，然后后面紧跟一个空格符号：

> 这是区块中的代码

## Markdown 代码

如果是段落上的一个函数或片段的代码可以用反引号把它包起来（`），例如：

```
function fn(a, b) {
    console.log("函数执行了", a, b);
    console.log("this", this);
}
```

## 链接(a)

这是一个链接 [百度](https://www.baidu.com)

## 图片

![alt 属性文本](../images/2.jpg)

## 表格

| 编号 | 姓名 | 班级 | 语文 | 数学 | 英语 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| 1    | 张三 | 2215 | 55   | 66   | 77   |
| 2    | 李四 | 2215 | 55   | 66   | 77   |
