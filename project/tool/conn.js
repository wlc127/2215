
let mysql = require("mysql");

function sqlQuery(sql){
    return new Promise(function(resolve,reject){
         // 创建连接 -> 连接mysql -> 配置信息  -> 返回连接对象
        var conn = mysql.createConnection({
            host:"localhost", // 本地主机
            port:'3306',   // 端口 默认3306
            user:"root",  // 登录的用户名
            password:"root", // 密码
            database:"2215"
        })
        conn.query(sql,function(err,results){
            if(err){
                reject({
                    message:err.sqlMessage,   // 错误信息
                    sql:err.sql, // 错误的sql语句
                });
            }else{
                resolve(results);
            }
            conn.end();
        })
    })
    
}

// mysql执行查询语句 -> 查询成功=>结果(数组)  查询失败-> [] 
var sql = "select id,name,class,chinese,math,english,chinese+math+english as total from grade where class = '2215'";

// sqlQuery(sql).then(list=>{
//     console.log("查询成功",list);
// }).catch(err=>{
//     console.log("查询失败",err);
// })

// mysql执行增删改 -> 结果对象(包含 affectedRows(受影响的行数) insertId: 新增的数据的id)
var sql = "insert into grade(name,class,chinese,math,english) values('张四','2215',55,66,77);"

// sqlQuery(sql).then(info=>{
//     console.log("查询成功",info);
// }).catch(err=>{
//     console.log("查询失败",err);
// })


module.exports =  {sqlQuery}


