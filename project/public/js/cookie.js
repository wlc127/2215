/**
 * 设置cookie (没有就新增 有就修改)
 *
 * @export
 * @param {*} key
 * @param {*} value
 * @param {*} expires
 * @param {string} [path="/"]
 */
export function setCookie(key, value, expires, path = "/") {
  if (expires) {
    var date = new Date();
    date.setSeconds(date.getSeconds() + expires);
    document.cookie =
      key + "=" + value + ";expires=" + date.toUTCString() + ";path=" + path;
  } else {
    document.cookie = key + "=" + value + ";path=" + path;
  }
}

export function getCookie(key) {
  var cookie = document.cookie;
  if (cookie) {
    var list = cookie.split("; ");
    for (var i = 0; i < list.length; i++) {
      var item = list[i]; // 每一条数据 'user=a123123'
      var name = item.split("=")[0]; // "user"
      var val = item.split("=")[1]; // "a123123"

      if (key == name) {
        return val;
      }
    }
  }
  return "";
}
