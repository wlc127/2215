// 对request进行二次封装 -> 一个接口对应一个功能 -> 起一个函数名 -> 调用该函数 -> 快速向指定接口发送请求

import { request } from "./ajax.js";

// 接口路径  /user/xxx

var baseUrl = "";

export let isExistUser = (data) =>
  request({
    type: "get",
    url: baseUrl + "/user/isExistUser",
    data: data,
    dataType: "json",
  });

export let isExistPhone = (data) =>
  request({
    type: "get",
    url: baseUrl + "/user/isExistPhone",
    data: data,
    dataType: "json",
  });
export let isExistEmail = (data) =>
  request({
    type: "get",
    url: baseUrl + "/user/isExistEmail",
    data: data,
    dataType: "json",
  });

export let register = (data) =>
  request({
    type: "post",
    url: baseUrl + "/user/register",
    data: data,
    dataType: "json",
  });

export let login = (data) =>
  request({
    type: "post",
    url: baseUrl + "/user/login",
    data: data,
    dataType: "json",
  });
export let loginAccount = (data) =>
  request({
    type: "post",
    url: baseUrl + "/demo/php/login_account.php",
    data: data,
    dataType: "json",
  });

export let searchGoodsOrderLimit = (data) =>
  request({
    type: "get",
    url: baseUrl + "/demo/php/searchGoodsOrderLimit.php",
    data: data,
    dataType: "json",
  });
