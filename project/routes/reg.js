var express = require('express');
var router = express.Router();

var list = [
    {
        "goodsId": "A001",
        "goodsName": "华为（HUAWEI） 笔记本电脑MateBook D16 16英寸高性能轻薄本商务办公学生手提超极本 银丨十二代i5-12500H 16G 512G",
        "goodsImg": "https://img12.360buyimg.com/n7/jfs/t1/195641/2/30490/89452/63745247Efb2a8608/46028f28c82c3faf.jpg",
        "goodsPrice": "5699.00",
        "commentCount": "2000+条评价",
        "shopName": "华为比特专卖店"
    },
    {
        "goodsId": "A002",
        "goodsName": "HUAWEI nova 8 SE 6400万高清四摄 支持66W超级快充 6.5英寸OLED大屏 8GB+128GB幻夜黑华为手机 标配无充",
        "goodsImg": "https://img11.360buyimg.com/n7/jfs/t1/172154/14/29890/38175/634d43e2E4b955033/0fd37f341d1a6186.jpg",
        "goodsPrice": "1199.00",
        "commentCount": "20万+条评价",
        "shopName": "华为京东自营官方旗舰店"
    },
    {
        "goodsId": "A003",
        "goodsName": "HUAWEI Mate 50 直屏旗舰 超光变XMAGE影像 北斗卫星消息 低电量应急模式 128GB曜金黑华为鸿蒙手机",
        "goodsImg": "https://img11.360buyimg.com/n7/jfs/t1/161463/36/32570/66358/636e661cE6ed2922d/cbcdc33ce6d66663.jpg",
        "goodsPrice": "4999.00",
        "commentCount": "2万+条评价",
        "shopName": "华为京东自营官方旗舰店"
    },
    {
        "goodsId": "A004",
        "goodsName": "华为（HUAWEI） 华为电视智慧屏86英寸 HarmonyOS 3 超大屏4K超高清液晶教育电视机 华为智慧屏S86 Pro(有摄像头)4+64G",
        "goodsImg": "https://img13.360buyimg.com/n7/jfs/t1/129886/38/33358/135433/6373051aEc175bf8b/3e56b288862216dc.jpg",
        "goodsPrice": "11999.00",
        "commentCount": "87+条评价",
        "shopName": "京东电器旗舰"
    },
    {
        "goodsId": "A005",
        "goodsName": "HUAWEI Pocket S 折叠屏手机 时尚多彩 40万次折叠认证 后摄人像自拍 256GB 冰霜银 华为小折叠pockets",
        "goodsImg": "https://img10.360buyimg.com/n7/jfs/t1/180779/4/30342/52096/6371f1d6E27b5cf53/59d4f937821167c0.jpg",
        "goodsPrice": "6488.00",
        "commentCount": "2000+条评价",
        "shopName": "京东电器旗舰"
    }
];

/* GET home page. */
// /reg
router.get('/', function (req, res, next) {

    // res.end("/reg");

    // 快捷创建页面路由: 
    // 1. 将需要的创建页面路由的文件 放到view文件夹中 -> 转化为.ejs
    // 2. ejs中如果要引入css js img 建议使用站点路径
    // 3. 通过res.render("ejs文件名", data); // 渲染指定ejs文件到页面 (传递数据到页面中)
    // res.render("register");

    // 渲染list.ejs 
    // res.render("list", {
    //     title: "hello list",
    //     isLogin: false,
    //     user: "a123123",
    // });

    // res.render("goods",{
    //     title:"商品的动态生成",
    //     list: list,
    // })
});

module.exports = router;
