var express = require('express');
var router = express.Router();

var { sqlQuery } = require("../tool/conn.js");

/* GET users listing. */
router.get('/', function (req, res, next) {
	res.send('respond with a resource');
});

// /user/register

// 接口书写完毕: 测试   
// get类型的接口: 直接打开浏览器测试
// post类型的接口:  http post机制传参 不好直接测试 
//    a. 先写成get 测试完毕 再改为post
//    b. 使用接口测试工具 -> postman / apipost

// /user/isExistUser  接收一个手机号名 判断用户名是否存在
router.get('/isExistUser', function (req, res, next) {
	var { user } = req.query;
	if (user) {
		var sql = `select * from userinfo where user = '${user}'`;
		sqlQuery(sql).then(list=>{ //查询的结果放到数组中
			if(list.length>0){// 有数据
				res.json({
					status:false,
					message:"该用户已经注册"
				})
			}else{ // 没有数据
				res.json({
					status:true,
					message:"可以使用的用户名"
				})
			}

		}).catch(err=>{
			res.json({
				status:false,
				...err,
			})
		})

	} else {
		res.json({
			status: "false",
			message: "请传入完整参数",
		})
	}
});


router.get('/isExistPhone', function (req, res, next) {
	var { phone } = req.query;
	if (phone) {
		var sql = `select * from userinfo where phone = '${phone}'`;
		sqlQuery(sql).then(list=>{ //查询的结果放到数组中
			if(list.length>0){// 有数据
				res.json({
					status:false,
					message:"该手机号已经注册"
				})
			}else{ // 没有数据
				res.json({
					status:true,
					message:"可以使用的手机号"
				})
			}

		}).catch(err=>{
			res.json({
				status:false,
				...err,
			})
		})

	} else {
		res.json({
			status: "false",
			message: "请传入完整参数",
		})
	}
});

router.get('/isExistEmail', function (req, res, next) {
	var { email } = req.query;
	if (email) {
		var sql = `select * from userinfo where email = '${email}'`;
		sqlQuery(sql).then(list=>{ //查询的结果放到数组中
			if(list.length>0){// 有数据
				res.json({
					status:false,
					message:"该邮箱已经注册"
				})
			}else{ // 没有数据
				res.json({
					status:true,
					message:"可以使用的邮箱"
				})
			}

		}).catch(err=>{
			res.json({
				status:false,
				...err,
			})
		})

	} else {
		res.json({
			status: "false",
			message: "请传入完整参数",
		})
	}
});


router.post('/register', function (req, res, next) {

	// req.query  以post方式接收前端传递的数据
	// req.body  以post方式接收前端传递的数据

	var { user, pwd, phone, email } = req.body;// 对应字段名接收数据

	if (user && pwd && phone && email) {

		var sql = `insert into userinfo(user,pwd,phone,email) values('${user}','${pwd}','${phone}','${email}')`;

		sqlQuery(sql).then(info => {
			if (info.affectedRows > 0) {
				res.json({
					status: true, //注册成功
					message: "注册成功",
				})
			} else {
				res.json({
					status: false, //注册失败
					message: "语句执行成功,但是注册失败",
				})
			}

		}).catch(err => { //  对象  {message,sql}
			res.json({
				status: false, //注册失败
				...err, // 将对象展开放到新的对象中
			})
		})


	} else { //有一个没有传
		res.json({
			status: "false",
			message: "请传入完整参数",
		})
	}
});

// /user/login -> 登录 
// 接收前端传递过来的user,pwd -> 从数据库中查询返回结果
router.post("/login",function(req,res,next){
	console.log(req.body);  // { user: 'a123123', pwd: '123123' }
	var {user,pwd} = req.body;
	if(user&&pwd){
		var sql = `select * from userinfo where user = '${user}'`;
		sqlQuery(sql).then(list =>{ // 查询的结果数据
			if(list.length>0){ //有数据
				var info = list[0];  //找到用户对应数据
				console.log("数据库信息",info);
				// 数据库中的密码和用户传入密码对比
				if(info.pwd == pwd){
					res.json({
						status: true,
						message: "登录成功",
					})
				}else{
					res.json({
						status: false,
						message: "用户名或密码有误",
					})
				}
			}else{ //没有数据
				res.json({
					status: false,
					message: "该用户未注册",
				})
			}

		}).catch(err=>{
			res.json({
				status:false,
				...err
			})
		})
	}else{
		res.json({
			status: "false",
			message: "请传入完整参数",
		})
	}
})

module.exports = router;
