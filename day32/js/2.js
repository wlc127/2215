var b = 1;

function addB() {
    b++;
    return b;
}

function reduceB() {
    b--;
    return b;
}

// ES6
// export { a,addA,reduceA}

// Node -> CommonJS
// console.log(module);

// module.exports.a = a;
// module.exports.addA = addA;
// module.exports.reduceA = reduceA;

module.exports = { b: b, addB, reduceB }

