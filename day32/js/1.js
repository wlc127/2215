var a = 1;

function addA() {
    a++;
    return a;
}

function reduceA() {
    a--;
    return a;
}

// ES6
// export { a,addA,reduceA}

// Node -> CommonJS
// console.log(module);

module.exports.a = a; // 1
module.exports.addA = addA; 
module.exports.reduceA = reduceA;

