
// require() 的路径 => 相对于当前文件
// CommonJS 模块输出的是一个值的拷贝，ES6 模块输出的是值的引用。
// CommonJS 模块是运行时加载，ES6 模块是编译时输出接口。
// CommonJS 模块的require()是同步加载模块，ES6 模块的import命令是异步加载，有一个独立的模块依赖的解析阶段。

// var obj1 = require("./1.js");
var { a, addA, reduceA } = require("./1.js");
console.log(a, addA, reduceA);

// var result = addA();
// console.log(result);


var { b, addB, reduceB } = require("./2.js");
console.log(b, addB, reduceB);

var c = require("./3.js");
console.log(c);
