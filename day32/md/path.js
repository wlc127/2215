
var path = require("path");
// console.log(path);

// 几个绝对路径
// console.log(__dirname);  // 返回当前文件目录
// console.log(__filename); // 返回当前文件路径
// console.log(process.cwd()); // 方法返回 Node.js 进程的当前工作目录。 => 终端启动的目录


// path.resolve([from ...], to) 将 to 参数解析为绝对路径，给定的路径的序列是从右往左被处理的，后面每个 path 被依次解析，直到构造完成一个绝对路径。

// var res = path.resolve(__dirname, "../js", "1.js");
// console.log(res);

// var res = path.resolve("one");
// var res = path.resolve(process.cwd(), "one");
// console.log(res);

// var res = path.resolve("one", "two");
// console.log(res);

// var res = path.resolve("/one", "/two");
// console.log(res);

// var res = path.resolve("/one", "two");
// console.log(res);


// path.join() 方法使用特定于平台的分隔符作为定界符将所有给定的 path 片段连接在一起，然后规范化生成的路径。

// var res = path.join(__dirname, "../js", "1.js");
// console.log(res);

// var res = path.join("one");
// console.log(res);

// var res = path.join("one", "two");
// console.log(res);

// var res = path.join("one", "/two");
// console.log(res);
// var res = path.join("one", "//");
// console.log(res);


var str = "C:/Users/怀霜凌志/Desktop/2215/day32/js/1.js";

// console.log(path.dirname(str));  // 目录部分
// console.log(path.basename(str)); // 文件部分
// console.log(path.extname(str));  // 后缀部分


// var result = path.parse(str); //返回路径字符串的对象。
// console.log(result);

