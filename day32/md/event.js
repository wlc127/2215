
var events = require("events");

// var EventEmitter = events.EventEmitter;
var { EventEmitter } = events;


var evt = new EventEmitter();
// console.log(evt);
// console.log(evt.__proto__);

// 添加事件
// evt.on(eventType,callback)
// evt.addListener(eventType,callback)
// evt.prependListener (eventName,callback)  用法同on 将事件监听器添加到监听器数组的开头 (此事件第一个执行)
// evt.once(eventType,callback)
// console.log(evt.on === evt.addListener);

// 触发事件
// evt.emit(eventType, data);

// 清除事件 -> 对应函数引用清除
// evt.off(eventType,callback)
// evt.removeListener(eventType,callback)

// 绑定事件
var a, b, c, d, e;
evt.on("a", a = (arg) => {
    console.log("这是a事件:1", arg);
})
evt.on("a", b = (arg) => {
    console.log("这是a事件:2", arg);
})
evt.addListener("a", c = (arg) => {
    console.log("这是a事件:3", arg);
})
evt.once("a", d = (arg) => {
    console.log("这是a事件:4", arg);
})
evt.prependListener("a", e = (arg) => {
    console.log("这是a事件:5", arg);
})

evt.prependListener("b", e = (arg) => {
    console.log("这是a事件:5", arg);
})


// 单个清除
// evt.off("a", a);
// evt.off("a", b);
// evt.off("a", c);
// evt.off("a", d);
// evt.off("a", e);

// evt.removeAllListeners("a");


// evt.emit("a", 1);
// evt.emit("a", 2);

// console.log(evt.listenerCount("a"));

// console.log(evt.eventNames());


console.log(evt.getMaxListeners());

evt.setMaxListeners(100);
console.log(evt.getMaxListeners());



