
var qs = require("querystring");

// qs.parse(str)
// str 需要解析的字符串
// separator 数据之间用什么分隔 (默认 &)
// eq        键值对之间用什么分隔 (默认 =)

// var str = "user=a123123&pwd=123123&email=123123@qq.com&phone=17386141517";
// var obj = qs.parse(str);
// console.log(obj);

// var str = "user-a123123; pwd-123123; email-123123@qq.com; phone-17386141517";
// var obj = qs.parse(str, "; ", "-");
// console.log(obj);

// var obj = {
//     user: 'a123123',
//     pwd: '123123',
//     email: '123123@qq.com',
//     phone: '17386141517'
// }

// var str = qs.stringify(obj);
// console.log(str);
// var str = qs.stringify(obj, "; ", "-");
// console.log(str);


// var str = "张三123abc+-*/%";
// var enStr = qs.escape(str);
// console.log(enStr);
// console.log(encodeURIComponent(str));

// var res = qs.unescape(enStr);
// console.log(res);
// console.log(decodeURIComponent(enStr));