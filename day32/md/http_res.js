
var http = require("http");
// var https = require("https");
// console.log(http);

var { createServer } = http;

// 创建一个服务并返回
var server = createServer(function (req, res) {
    // 每次前端向后端发送请求时 都会触发次回调函数
    // req (request) 请求相关的数据 (请求方式 请求地址 请求头信息)
    // res (response) 响应的相关信息 (定义状态码 响应数据类型 响应正文)
    console.log("接收到前端发送的请求");

    // res.write() 写入响应数据(可以有多次)

    // text
    // res.write("welcome ");
    // res.write("to ");
    // res.write("my ");
    // res.write("node ");
    // res.write("server ");

    // 通过响应头设置数据类型 (响应头 键值对的对象数据)
    // res.setHeader("Content-Type", "text/plain;charset=utf-8"); //文本类型
    // res.setHeader("Content-Type", "text/html;charset=utf-8"); //html
    // res.setHeader("Content-Type", "text/css;charset=utf-8"); //css
    // res.setHeader("Content-Type", "text/javscript;charset=utf-8"); //javscript
    // res.setHeader("Content-Type", "text/json;charset=utf-8"); //json
    // res.setHeader("Content-Type", "image/png"); //图片

    // res.statusCode = 404;
    // res.statusMessage = "1111";

    // html
    // res.writeHead(200, {
    //     "Content-Type": "text/html;charset=utf-8"
    // });
    // res.write("<h2>hello everyone!</h2>");

    // json
    res.writeHead(200, {
        "Content-Type": "text/json;charset=utf-8",
        "a": 1,
        "b": 2,
    });
    var obj = { name: "张三", age: 18 };
    res.write(JSON.stringify(obj));


    res.end();

    // res.end("welcome to my node server");  //响应结束(可以携带数据)
});

// server.listen(port,host,callback)
// port  端口
// host  主机名 (默认本地主机 localhost)
// callback 服务启动时的回调函数

server.listen(3000, "localhost", () => {
    console.log("Server Start at localhost:3000");
});

