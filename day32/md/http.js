
var http = require("http");
// var https = require("https");
// console.log(http);

var { createServer } = http;

// 创建一个服务并返回
var server = createServer(function (req, res) {
    // 每次前端向后端发送请求时 都会触发次回调函数
    // req (request) 请求相关的数据 (请求方式 请求地址 请求头信息)
    // res (response) 响应的相关信息 (定义状态码 响应数据类型 响应正文)
    console.log("接收到前端发送的请求");

    // 请求相关的信息:
    // console.log(req);
    // console.log(req.method); // 请求方式
    console.log(req.url); // 请求地址
    // console.log(req.headers); // 请求头信息




    res.end();

    // res.end("welcome to my node server");  //响应结束(可以携带数据)
});

// server.listen(port,host,callback)
// port  端口
// host  主机名 (默认本地主机 localhost)
// callback 服务启动时的回调函数

server.listen(3000, "localhost", () => {
    console.log("Server Start at localhost:3000");
});

