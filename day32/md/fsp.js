
var fsp = require("fs/promises");  //高版本
// console.log(fsp);

var fs = require("fs");  //低版本
var fsp = fs.promises;
// console.log(fsp);

var path = require("path");


fsp.readFile(path.join(__dirname, "../data/2.txt"), "utf-8").then(data => {
    console.log("读取成功!");
}).catch(err => {
    console.log("读取失败!", err);
})
