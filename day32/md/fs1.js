
var fs = require("fs");
var path = require("path");
// console.log(fs);

// file 文件
// directory  文件夹

// fs.mkdir(path.join(__dirname, "../data/1111"), (err) => {
//     if (err) throw err;
//     console.log("文件夹创建成功");
// })


// 强调 -> 只有空文件夹才能直接被删除 -> 否则需要先删除文件夹中的内容,在删除文件夹
// fs.rmdir(path.join(__dirname, "../data/1111"), (err) => {
//     if (err) throw err;
//     console.log("文件夹删除成功");
// })

// fs.rmdir(path.join(__dirname, "../data"), (err) => {
//     if (err) throw err;
//     console.log("文件夹删除成功");
// })


// readdir 读取目录
// list 目录下的文件信息
// fs.readdir(path.join(__dirname, "../data"), (err, list) => {
//     if (err) throw err;
//     console.log("文件夹读取成功", list);
// })


// fs.stat()  获取文件/目录的相关信息
fs.stat(path.join(__dirname, "../data"), (err, info) => {
    if (err) throw err;
    // console.log(info);
    console.log(info.isFile());  // 是不是文件
    console.log(info.isDirectory());  // 是不是文件夹

})

