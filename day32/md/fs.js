
var fs = require("fs");
var path = require("path");
// console.log(fs);

// url -> 读取的路径 (相对路径 -> 默认相对于process.cwd() )
// fs.readFile(path.join(__dirname, "../js/1.js"), "utf-8", (err, data) => {
//     if (err) throw err;
//     console.log("文件读取成功", data);
// })

// fs.readFile(path.join(__dirname, "../data/1.json"), "utf-8", (err, data) => {
//     if (err) throw err;
//     console.log("文件读取成功", data);
// })

// fs.readFile(path.join(__dirname, "../data/2.jpg"), "binary", (err, data) => {
//     if (err) throw err;
//     console.log("文件读取成功", data);
// })


// 写入
// fs.writeFile(url,data,options,callback)
// fs.writeFile(path.join(__dirname, "../data/1.txt"), "Hello EveryOne,把眼睛睁开!", {
//     encoding: "utf8",
//     flag: "w",
// }, (err) => {
//     if (err) throw err;
//     console.log("写入成功");
// })

// fs.appendFile(path.join(__dirname, "../data/1.txt"), "NodeJS 太难了!", {
//     encoding: "utf8",
//     flag: "w",
// }, (err) => {
//     if (err) throw err;
//     console.log("写入成功");
// })

// fs.unlink(path.join(__dirname, "../data/1.txt"), (err) => {
//     if (err) throw err;
//     console.log("删除成功");
// })

// fs.existsSync()  判断文件和文件夹是否存在
// console.log(fs.existsSync(path.join(__dirname, "../data/1.json")));
// console.log(fs.existsSync(path.join(__dirname, "../data/1.txt")));

// 重命名文件和文件夹
// var isExist = fs.existsSync(path.join(__dirname, "../data/1.txt"));
// if (isExist) {
//     fs.rename(path.join(__dirname, "../data/1.txt"), path.join(__dirname, "../data/2.txt"), (err) => {
//         if (err) throw err;
//         console.log("修改成功");
//     })
// }


