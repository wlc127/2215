
 function ajax(options) { // 对象  {url,data,async,dataType,success}

        var { type = "get", url, data = "", async = true, dataType = "text", success } = options;

        var xhr = new XMLHttpRequest();

        if (Object.prototype.toString.call(data) == "[object Object]") {
            var list = [];
            for (var key in data) {
                var val = data[key];
                list.push(`${key}=${val}`);
            }
            data = list.join("&");
        }
        // console.log(data);

        if (type.toLowerCase() == "get") {
            xhr.open(type, data ? (url + "?" + data) : url, async);
            xhr.send();
        } else if (type.toLowerCase() == "post") {
            xhr.open(type, url, async);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.send(data);
        }

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {
                var result = xhr.responseText; //获取响应的文本
                if (dataType == "json") {
                    result = JSON.parse(result);
                }
                // console.log(result);

                if (success && typeof success == "function") {
                    success.call(xhr, result); //请求成功时执行 传入响应数据
                }
            }
        }
}


// request(options)  调用此方法返回一个Promise对象(pending) -> 等请求成功之后(fulfilled)
function request(options) { // 对象  {url,data,async,dataType,success}
    return new Promise(function (resolve, reject) { 
        var { type = "get", url, data = "", async = true, dataType = "text" } = options;

        var xhr = new XMLHttpRequest();

        if (Object.prototype.toString.call(data) == "[object Object]") {
            var list = [];
            for (var key in data) {
                var val = data[key];
                list.push(`${key}=${val}`);
            }
            data = list.join("&");
        }
        // console.log(data);

        if (type.toLowerCase() == "get") {
            xhr.open(type, data ? (url + "?" + data) : url, async);
            xhr.send();
        } else if (type.toLowerCase() == "post") {
            xhr.open(type, url, async);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.send(data);
        }

        xhr.onreadystatechange = function () {

            if (xhr.readyState == 4 && xhr.status == 200) {
                var result = xhr.responseText; //获取响应的文本
                if (dataType == "json") {
                    result = JSON.parse(result);
                }
                
                resolve(result);
            }
        }
    })
      
}
    
export { ajax ,request};