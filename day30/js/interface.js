// 对request进行二次封装 -> 一个接口对应一个功能 -> 起一个函数名 -> 调用该函数 -> 快速向指定接口发送请求

import { request } from "./ajax.js";

// export function isExistUser(data) {
//   return request({
//     type: "get",
//     url: "http://43.138.81.225/demo/php/isExistUser.php",
//     data: data,
//     dataType: "json",
//   });
// }

// export function register(data) {
//   return request({
//     type: "post",
//     url: "http://43.138.81.225/demo/php/register.php",
//     data: data,
//     dataType: "json",
//   });
// }

// export function login(data) {
//   return request({
//     type: "post",
//     url: "http://43.138.81.225/demo/php/login.php",
//     data: data,
//     dataType: "json",
//   });
// }

var baseUrl = "http://43.138.81.225";

export let isExistUser = (data) =>
  request({
    type: "get",
    url: baseUrl + "/demo/php/isExistUser.php",
    data: data,
    dataType: "json",
  });

export let isExistPhone = (data) =>
  request({
    type: "get",
    url: baseUrl + "/demo/php/isExistPhone.php",
    data: data,
    dataType: "json",
  });
export let isExistEmail = (data) =>
  request({
    type: "get",
    url: baseUrl + "/demo/php/isExistEmail.php",
    data: data,
    dataType: "json",
  });

export let register = (data) =>
  request({
    type: "post",
    url: baseUrl + "/demo/php/register.php",
    data: data,
    dataType: "json",
  });

export let login = (data) =>
  request({
    type: "post",
    url: baseUrl + "/demo/php/login.php",
    data: data,
    dataType: "json",
  });
export let loginAccount = (data) =>
  request({
    type: "post",
    url: baseUrl + "/demo/php/login_account.php",
    data: data,
    dataType: "json",
  });

export let searchGoodsOrderLimit = (data) =>
  request({
    type: "get",
    url: baseUrl + "/demo/php/searchGoodsOrderLimit.php",
    data: data,
    dataType: "json",
  });
