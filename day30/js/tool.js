var numList = [];
for (var i = 48; i <= 57; i++) {
    var char = String.fromCharCode(i);
    numList.push(char);
}

var bigList = [];
for (var i = 65; i <= 90; i++) {
    var char = String.fromCharCode(i);
    bigList.push(char);
}

var smallList = [];
for (var i = 97; i <= 122; i++) {
    var char = String.fromCharCode(i);
    smallList.push(char);
}

var speList = ["_"]; //允许使用的特殊字符

var normalList = numList.concat(bigList, smallList, speList);
console.log(normalList);



function randCode(len){
    // console.log("随机一次");
    if(len==undefined) len = 4;  //不传参
    if(len < 4) len = 4;
    var codeList = numList.concat(bigList,smallList);
    var arr = [];
    for(var i = 0; i < len; i++){  // 加入i=2时重复
        // var index = Math.round(Math.random()*(codeList.length-1));  //下标的最大值 = length-1
        var index = Math.floor(Math.random()*codeList.length);  //下标的最大值 = length-1
        var code = codeList[index];
        if(!arr.includes(code)){  // 新数组中没有就放置
            arr.push(code)
        }else{ // 已存在
            i--; // i-- => 1   本次循环结束 i++  => 2 (本次不算)
        }
    }


    var isExist = {
        num:false,
        big:false,
        small:false,
    }

    for(var char of arr){ //遍历arr 获取每个字符
        if(numList.includes(char)){ // 如果在numList中存在 => 是数字
            isExist.num = true;
            continue;
        }

        if(bigList.includes(char)){
            isExist.big = true;
            continue;
        }

        if(smallList.includes(char)){
            isExist.small = true;
            continue;
        }

        if(speList.includes(char)){
            isExist.spe = true;
            continue;
        }
    }

    var level = 0;
    for(var key in isExist){ // 3
        var val = isExist[key];
        level += val;
    }

    if(level==3){
        return arr.join("");
    }else{
        return randCode(len);
    }
}

export {randCode}